# PGDM1400–1412 antibody sequences from Sok 2014

Recombinant HIV envelope trimer selects for quaternary-dependent antibodies targeting the trimer apex.
Sok et al.
PNAS 111 (49) 17624-17629
<https://doi.org/10.1073/pnas.1415789111>
