#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from collections import defaultdict
from csv import DictReader, DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    match = re.match(r"([A-Z0-9.]+) .*isolate (PGDM[0-9]+[HK]C) .*immunoglobulin (heavy|light) chain.*", txt)
    grps = match.groups()
    keys = ["Accession", "Antibody", "Chain"]
    fields = {k: v for k, v in zip(keys, grps)}
    fields["Antibody"] = fields["Antibody"][:-2]
    return fields

FIELDS = ["Antibody", "HeavyAccession", "LightAccession", "HeavySeq", "LightSeq"]

def make_seqs_sheet(fastas):
    mabs = defaultdict(dict)
    for fasta in fastas:
        for record in SeqIO.parse(fasta, "fasta"):
            fields = parse_seq_desc(record.description)
            col_acc = fields["Chain"].capitalize() + "Accession"
            col_seq = fields["Chain"].capitalize() + "Seq"
            mabs[fields["Antibody"]]["Antibody"] = fields["Antibody"]
            mabs[fields["Antibody"]][col_acc] = fields["Accession"]
            mabs[fields["Antibody"]][col_seq] = str(record.seq)
    mabs = list(mabs.values())
    mabs.sort(key = lambda row: row["Antibody"])
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(mabs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
